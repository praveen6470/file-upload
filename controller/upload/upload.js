const multer = require("multer");
const connection = require("../../config/config");
const helperFun = require("../../helpers/helper");

const fileUpload = (req, res, next) => {
  const file = req.file;
  if (!req?.file) {
    return res.status(405).send({ message: "Please upload a file." });
  } else {
    const query =
      "insert into Document(UniqueIDGUID, EntityID, DocID, DocOriginalName, DocType, DocPath) values('" +
      helperFun.uuid() +
      "', '" +
      req?.body?.EntityID +
      "', '" +
      helperFun.guid() +
      "', '" +
      req?.file?.originalname +
      "', '" +
      req?.file?.mimetype?.split("/")[1] +
      "', '" +
      req?.file?.path +
      "')";
    connection.db.query(query, (err, result) => {
      if (err) {
        res.status(500).json({ message: err.message });
      } else {
        res.json({ status: 200, message: "Uploaded Successfully" });
      }
    });
  }
};

const fetchFiles = (req, res, next) => {
  const query = "select * from Document ORDER BY DocOriginalName ASC";
  connection.db.query(query, (err, result) => {
    if (err) {
      res.json({ error: err.message });
    } else {
      res.json({ status: 200, data: result });
    }
  });
};

module.exports = {
  fileUpload,
  fetchFiles,
};
