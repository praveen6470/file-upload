const express = require("express"); //Line 1
const connection = require("./config/config");
const app = express(); //Line 2
const port = process.env.PORT || 5001; //Line 3
const cors = require("cors"); //use this
const path = require("path");
const multer = require("multer");
const files = require("./controller/upload/upload");


connection.db.connect((err) => {
  if (err) {
    throw err;
  }
  console.log("Connected to database document");
});
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

let storage = multer.diskStorage({
   destination: function (req, file, cb) {
      cb(null, 'uploads');
   },
   filename: function (req, file, cb) {
      cb(null, `${file.originalname}${path.extname(file.originalname)}`);
   }
});
let upload = multer({ storage: storage });
app.use(cors());
app.use('/uploads', express.static('uploads'));
// create a post route
app.post("/dcoumentUpload", upload.single('file'),  files.fileUpload);
app.get("/dcoumentUpload", files.fetchFiles);
app.listen(port, () => console.log(`Listening on port ${port}`)); //Line 6
